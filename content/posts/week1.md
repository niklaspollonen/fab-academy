---
title: "Week1"
date: 2020-02-03T12:05:34+02:00
draft: false
---
Week 1 - Making a hugo site and deplying it on GitLab pages. 

This blog is managed with Git and Hugo. Git is a version control software and Hugo is a static site generator. Git manages different versions of data while also storing the old versions. Therefore you can go back to older version, compare them with the current one, make different branches of code, join them etc.

GitLab is a webapplication that uses a Git. The documentation during Fab Academy is on Gitlab, that has also other functionality as, for example, deplaying the code and showing it as a website. This post describes how I deplayed my site for Fab Academy.

Command line was be used for uploading the code through Git software, and the commands are described below. GitLab can also be accessed using SSH(secure shell), but I did not find this necessary for this project. 

I started by creating a new project in GitLab with the web interface and copied the http address from for the new repo. The repo was cloned it to my local computer with the command: 


```
git clone https://gitlab.com/username/repo-name.git

```
To generate the site I decided to try Hugo for generating static pages. I used the following tutorial that explains how to make a new repo and upload it to github: 
https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/ 

I am using OSX and Homebrew, therefore the installation could be done by writing the following in Termial app: 
```
brew install hugo
```
If you do not  have homebrew, install it with the command: 
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Instead of starting a new repo for the Hugo project, I used the command: 
```
hugo new site repo-name -force
```

The "-force" forces the creation of a new project even if a folder with the same name already exists, this way I was able to make the hugo project on top of the GitLab repo that already excisted. 

Then I installed the hugo theme namned Ananke, installation instructions was found here: 
https://themes.gohugo.io/gohugo-theme-ananke/ . 
And also did a few more commands to prepare my hugo site.

-   Created a new post for week1 with:
```
hugo new /posts/week1 
```
-   Added the build folder to gitignore: 
```
echo "/public" >> .gitignore 
```
- Edited the main site by first copying index.html from themes/ananke/layouts/index.html to layouts/index.html, and then editing the file too include some code. I also removed the "recent posts" title text from the template to simplify the main page.

- created a .gitlab-ci.yml file by terminal command 
```
touch .gitlab-ci.yml
```
And then copied the content provided by the tutorial:
### .gitlab-ci.yml
```
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

# Pushing to GitLab

Then it was time to update the content to GitLab!
I used the following commands
```
git add .
git commit -m "First week, first post"
git push -u origin master
```




