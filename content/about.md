---
title: "About"
menu: "main"
draft: false
---

## Niklas Pöllönen
I have been working as an adhoc at Aalto Fablab for few years, therefore I have experience with most of the machinery and tools available. I also work as a freelancer in propgramming, design, 3D CAD, prototyping etc. 

I mostly do the small prototypes in my small workshop where I have some equpment as 3D printers, CNC-mill and hand tools. 

I'm interested in robotics, creative coding and Art. Because my work is my interest, I also use most of my free time working. 
